export type SortTypes = 'asc' | 'desc';
export type TplTypes = 'internal' | 'extended' | 'external';
export interface NgiSelectOptions {
    multiple?: boolean;
    type?: TplTypes;
    sort?: SortTypes;
}
export interface FilteredCollection {
    value: string;
    isSelected?: boolean;
}
