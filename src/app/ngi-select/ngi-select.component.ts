import {
    Component,
    OnInit,
    AfterViewInit,
    AfterViewChecked,
    EventEmitter,
    Input,
    Output,
    HostListener,
    ViewChild,
    ElementRef,
} from '@angular/core';
import { NgiSelectOptions } from './ngi-select.types';

@Component({
    selector: 'ngi-select',
    templateUrl: './ngi-select.component.html',
    styleUrls: ['./ngi-select.component.sass']
})
export class NgiSelectComponent implements OnInit, AfterViewInit, AfterViewChecked {

    minSearchLength = 0;
    inputValue: string;
    filteredCollection: string[] = [];
    parentElement: HTMLElement;
    hostElement: HTMLElement;
    selectInputElement: HTMLElement;
    hostRect: ClientRect;
    selectContainerElement: HTMLElement;

    selectedItems: string[] = [];
    result: string | string[] = [];

    private isSelectOpened: boolean;

    /**
     * Получает коллекцию для отображения в селекторе
     */
    @Input() collection: Array<string>;

    /**
     * Получает опции (настройки) селектора
     */
    @Input() options: NgiSelectOptions;

    @Output() selectedCollection = new EventEmitter<string | string[]>();
    @ViewChild('selectContainer') selectContainer: ElementRef;
    @ViewChild('selectInput') selectInput: ElementRef;

    @HostListener('window:resize')
    onWindowResize() {
        this.positionElement();
    }
    @HostListener('document:click', ['$event'])
    onDocumentClick(event: MouseEvent) {
        if (!(<Node>this.hostElement).contains(<Node>event.target)
            && !(<Node>this.selectContainerElement).contains(<Node>event.target)) {
                this.closeSelect();
        }
    }
    @HostListener('document:keydown', ['$event'])
    onKeydown(event: KeyboardEvent) {
        if (event.key === 'Escape' && this.isSelectOpened === true) {
            this.closeSelect();
            this.selectInputElement.blur();
        }
    }

    constructor(
        private host: ElementRef,
    ) {
        if (!this.options) {
            this.options = {};
        }
        if (!this.options.multiple) { this.options.multiple = false; }
    }

    /**
     * Входная функция, осуществляющая распределение логики в зависимости от переданных параметров настройки
     */
    onValueChange(value: string) {
        this.openSelect();
        this.inputValue = value;
        if (value.length >= this.minSearchLength) {
            this.filterCollection();
        }
    }

    /**
     * Фильтрует коллекцию по поисковой строке
     * Формирует новый массив filteredCollection
     */
    filterCollection() {
        this.filteredCollection = this.collection.filter((item: string) => {
            return item.toLowerCase().includes(this.inputValue.toLowerCase());
        });
    }

    /**
     * Раскрывает (отображает) контейнер коллекции
     */
    openSelect() {
        this.selectContainerElement.style.display = 'block';
        this.isSelectOpened = true;
    }

    /**
     * Скрывает контейнер коллекции
     */
    closeSelect() {
        this.selectContainerElement.style.display = 'none';
        this.isSelectOpened = false;
    }

    /**
     * Переключает видимость контейнера коллекции в зависимости от ее состояния
     */
    toggleSelect() {
        if (this.isSelectOpened) {
            this.closeSelect();
        } else {
            this.openSelect();
            this.setFocus();
        }
    }

    /**
     * Отрабатывает клик по элементу списка
     */
    selectItem(item: string, index: number) {
        this.inputValue = '';
        this.filterCollection();
        if (this.options.multiple) {
            (<string[]>this.result).push(item);
            this.setFocus();
        } else {
            this.result = item;
            this.inputValue = item;
            this.setFocus();
            this.closeSelect();
        }
        this.selectedCollection.emit(this.result);
    }

    resetValues() {
        this.inputValue = '';
        if (this.options.multiple) {
            this.result = [];
        } else {
            this.result = '';
        }
        this.filterCollection();
        this.setFocus();
        this.selectedCollection.emit(this.result);
    }

    setFocus() {
        this.selectInputElement.focus();
    }

    getCls(item: string): boolean {
        for ( let i = 0; i < (<string[]>this.result).length; i++ ) {
            if ( this.result[i] === item ) {
                return true;
            }
        }
        return false;
    }

    get caretDirection(): string {
        if (this.isSelectOpened) {
            return 'fa-caret-up';
        } else {
            return 'fa-caret-down';
        }
    }

    ngOnInit() {
        this.hostElement = this.host.nativeElement;
        this.selectInputElement = this.selectInput.nativeElement;
        if (this.options.sort) {
            switch (this.options.sort) {
                case 'asc':
                    this.filteredCollection = this.collection.sort(this.sortAsc);
                    break;
                case 'desc':
                    this.filteredCollection = this.collection.sort(this.sortDesc);
                    break;
            }
        }
        this.filteredCollection = this.collection;
    }

    ngAfterViewInit() {
        this.selectContainerElement = this.selectContainer.nativeElement;
        this.parentElement = document.body;
        // Назначаем контейнер с селектором в тэг body
        this.parentElement.appendChild(this.selectContainerElement);
        this.positionElement();
        this.selectContainerElement.style.display = 'none';
    }

    ngAfterViewChecked() {
        this.positionElement();
    }

    /**
     * Перепозиционирует элемент
     */
    private positionElement() {
        this.hostRect = this.hostElement.getBoundingClientRect();
        this.selectContainerElement.style.top = `${this.hostRect.bottom}px`;
        this.selectContainerElement.style.left = `${this.hostRect.left}px`;
        this.selectContainerElement.style.width = `${this.hostRect.width}px`;
    }

    /**
     * Устанавливает алгоритм сортировки по возрастанию
     * Для передачи в функцию sort()
     */
    private sortAsc(a: string, b: string): number {
        if (a > b) { return 1; }
        if (a < b) { return -1; }
        return 0;
    }

    /**
     * Устанавливает алгоритм сортировки по убыванию
     * Для передачи в функцию sort()
     */
    private sortDesc(a: string, b: string): number {
        if (a > b) { return -1; }
        if (a < b) { return 1; }
        return 0;
    }
}
