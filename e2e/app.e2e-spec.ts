import { Ng2InterfacesPage } from './app.po';

describe('ng2-interfaces App', function() {
  let page: Ng2InterfacesPage;

  beforeEach(() => {
    page = new Ng2InterfacesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
